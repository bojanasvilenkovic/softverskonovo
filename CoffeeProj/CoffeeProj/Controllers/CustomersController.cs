﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoffeeProj.Models;
using System.Data.Entity;
using CoffeeProj.ViewModel;

namespace CoffeeProj.Controllers
{
    public class CustomersController : Controller
    {
        private ApplicationDbContext _context;

        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        public ViewResult Index()
        {
            var customers = _context.Customers.Include(c => c.MembershipType).ToList();
            return View(customers);
          //  return View();
        }
        

        //public ActionResult Index(int? pageIndex, string sortBy)
        //{
        //    return View();

        //    //if (!pageIndex.HasValue)
        //    //    pageIndex = 1;
        //    //if (string.IsNullOrWhiteSpace(sortBy))
        //    //    sortBy = "Name";

        //    //return Content(string.Format("pageIndex={0}&sortBy={1}", pageIndex, sortBy));

        //}

        public ActionResult Details(int id)
        {

            var customer = _context.Customers.Include(c => c.MembershipType).SingleOrDefault(c => c.Id == id);

            if (customer == null)
                return HttpNotFound();

            return View(customer);

        }

        public ActionResult CustomerForm()
        {
            return View(); 

            //var customers = new Customers() { Name = "Neko" };

            //var viewModel = new CustomerFormViewModel
            //{
                
            //    Customer = customers
            //};
            //return View();
        }

       


    }
}