﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoffeeProj.Models;
using CoffeeProj.ViewModel;


namespace CoffeeProj.Controllers
{
    public class CoffeeController : Controller
    {
        // GET: Coffee   /Coffee/Random
        public ActionResult Random()
        {
            var coffee = new Coffee() { name = "Expresso" };
            var customers = new List<Customers>
            {
                new Customers { Name= "Customer 1"},
                new Customers { Name= "Customer 2"}
            };

            var viewModel = new RandomCoffeeViewModel
            {
                Coffee = coffee,
                Customers = customers
            };

            return View(viewModel);

        }

        public ActionResult Edit(int id)
        {
            return Content("id= " + id);

        }

        //coffee

        public ActionResult Index(int? pageIndex, string sortBy)
        {
            //if (!pageIndex.HasValue)
            //    pageIndex = 1;
            //if (string.IsNullOrWhiteSpace(sortBy))
            //    sortBy = "Name";

            //return Content(string.Format("pageIndex={0}&sortBy={1}", pageIndex, sortBy));
            return View();

        }

        [Route("coffee/released/{id}/{price:regex(\\d{4}):range(1, 12)}")]
        public ActionResult ByReleasePrice(int id, int price)
        {
            return Content(id + "/" + price);
        }
    }
}