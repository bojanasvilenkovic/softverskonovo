﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CoffeeProj.Startup))]
namespace CoffeeProj
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
