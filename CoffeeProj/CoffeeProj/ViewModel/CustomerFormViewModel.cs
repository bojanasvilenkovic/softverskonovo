﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoffeeProj.Models;

namespace CoffeeProj.ViewModel
{
    public class CustomerFormViewModel
    {
        public IEnumerable<MembershipType> MembershipTypes { get; set; }
        public Customers Customer { get; set; }
    }
}