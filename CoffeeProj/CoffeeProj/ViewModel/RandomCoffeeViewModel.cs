﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoffeeProj.Models;


namespace CoffeeProj.ViewModel
{
    public class RandomCoffeeViewModel
    {
        public Coffee Coffee { get; set; }
        public List<Customers> Customers { get; set; }
    }
}